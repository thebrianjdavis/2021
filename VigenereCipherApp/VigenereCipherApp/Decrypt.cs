﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigenereCipherApp
{
    class Decrypt
    {
        public static void decrypt(string userText, string eKey)
        {
            int eKeyIteration = 0;
            int convertChar;
            string outputText = "";
            userText = userText.ToUpper();
            eKey = eKey.ToUpper();

            foreach (char t in userText)
            {
                if (t < 65) continue;
                convertChar = t - 65 + -1 * (eKey[eKeyIteration] - 65);

                if (convertChar < 0) convertChar += 26;
                outputText += Convert.ToChar(65 + (convertChar % 26));

                if (++eKeyIteration == eKey.Length) eKeyIteration = 0;
            }

            Console.WriteLine(outputText);
        }
    }
}
