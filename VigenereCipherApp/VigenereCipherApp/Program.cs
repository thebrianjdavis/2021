﻿using System;

namespace VigenereCipherApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TextPrompts.AppHeader();
            TextPrompts.Introduction();

            bool isAppRunning = true;

            while (isAppRunning)
            {
                TextPrompts.AppHeader();
                TextPrompts.MainMenu();
                string menuSelection = null;
                string eKey = null;
                string userText = null;
                menuSelection = Console.ReadLine();
                Console.Clear();

                if (menuSelection == "1")
                {
                    TextPrompts.AppHeader();
                    TextPrompts.EncryptPrompt();
                    eKey = Console.ReadLine();
                    Console.Clear();

                    TextPrompts.AppHeader();
                    Console.WriteLine($"The encryption key has been set to: \n{eKey}\n");
                    TextPrompts.RetToCon();

                    TextPrompts.AppHeader();
                    Console.WriteLine("Enter text to be encrypted: \n");
                    userText = Console.ReadLine();
                    Console.Clear();

                    TextPrompts.AppHeader();
                    Console.WriteLine("Your encrypted message: \n");
                    Encrypt.encrypt(userText, eKey);
                    Console.WriteLine();
                    TextPrompts.RetToCon();

                }
                else if (menuSelection == "2")
                {
                    TextPrompts.AppHeader();
                    TextPrompts.DecryptPrompt();
                    eKey = Console.ReadLine();
                    Console.Clear();

                    TextPrompts.AppHeader();
                    Console.WriteLine($"The encryption key has been set to: \n{eKey}\n");
                    TextPrompts.RetToCon();

                    TextPrompts.AppHeader();
                    Console.WriteLine("Enter text to be encrypted: \n");
                    userText = Console.ReadLine();
                    Console.Clear();

                    TextPrompts.AppHeader();
                    Console.WriteLine("Your decrypted message: \n");
                    Decrypt.decrypt(userText, eKey);
                    Console.WriteLine();
                    TextPrompts.RetToCon();
                }
                else if (menuSelection == "3")
                {
                    TextPrompts.AppHeader();
                    TextPrompts.ByeBye();
                    isAppRunning = false;
                    break;
                }
                else if (menuSelection == "69" || menuSelection == "420")
                {
                    TextPrompts.AppHeader();
                    TextPrompts.Funny();
                }
                else
                {
                    TextPrompts.AppHeader();
                    TextPrompts.Oops();
                }
            }
        }
    }
}
