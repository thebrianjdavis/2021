﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigenereCipherApp
{
    class TextPrompts
    {
        public static void AppHeader()
        {
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            Console.WriteLine("%%%%%%%% Vigenere Cipher v1.0 %%%%%%%%%");
            Console.WriteLine("%%%%%%%%%%% by Brian Davis %%%%%%%%%%%%");
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        }
        public static void Introduction()
        {
            Console.WriteLine("This application will encode and decode");
            Console.WriteLine("messages using a Vigenere Cipher!");
            Console.WriteLine("\nInput is not case-sensitive, but is");
            Console.WriteLine("encoded or decoded in all upper-case to");
            Console.WriteLine("a single string.\n");
            Console.WriteLine("Numbers are currently not supported");
            Console.WriteLine("and will not be included in output.\n");
            RetToCon();
        }

        public static void MainMenu()
        {
            Console.WriteLine("Please choose from the menu below:");
            Console.WriteLine("Enter (1) to encode an outgoing message");
            Console.WriteLine("Enter (2) to decode an incoming message");
            Console.WriteLine("Enter (3) to exit the program");
        }

        public static void EncryptPrompt()
        {
            Console.WriteLine("You have selected to encode an outgoing\nmessage");
            Console.Write("\nPlease enter the encryption key (password): \n");
        }

        public static void DecryptPrompt()
        {
            Console.WriteLine("You have selected to decode an incoming\nmessage");
            Console.Write("\nPlease enter the encryption key (password): \n");
        }

        public static void RetToCon()
        {
            Console.WriteLine("Press RETURN to continue...");
            Console.ReadLine();
            Console.Clear();
        }

        public static void Funny()
        {
            Console.WriteLine("YOU'RE HILARIOUS!!!");
            Console.WriteLine("\nToo bad that funny humans just get sent\nback to the main menu...\n");
            RetToCon();
        }
        public static void Oops()
        {
            Console.WriteLine("Oops!! Invalid selection!!\nGoing to main menu...\n");
            RetToCon();
        }
        public static void ByeBye()
        {
            Console.WriteLine("Press return to exit Vigenere Cipher app");
            Console.WriteLine("Have a great day!");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
